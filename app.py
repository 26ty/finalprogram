from flask import Flask, render_template, request, redirect, url_for
from flask import session
from database import db_session, init_db
from models.database import People_account, Product_nomal
from sqlalchemy import desc
import os
app = Flask(__name__)

init_db()
#產生db檔
#設置金鑰
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

@app.route('/')
def index():
    product_nomal = Product_nomal.query.all()
    return render_template('index.html',product_nomal=product_nomal)

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/sighin')
def sighin():
    return render_template('sighin.html')

@app.route('/nomal-product-add',methods=['GET','POST'])
def nomalproductadd():
    error=""
    if request.method == 'POST':
        product_nomal = Product_nomal(
            name=request.args.get("name",""),
            decription=request.args.get("decription",""),
            price=request.args.get("price",""),
            img=request.args.get("img",""),
            unit=request.args.get("unit","")
        )
        db_session.add(product_nomal)#SQLite新增
        db_session.commit()#存入
        return error
    return render_template('nomal-product-add.html')

@app.route('/nomal-product')
def nomalProduct():
    return render_template('nomal-product.html')

@app.route('/custom-product')
def customProduct():
    return render_template('custom-product.html')

# Run App
if __name__ == "__main__":
    app.jinja_env.auto_reload = True
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
    )

#test