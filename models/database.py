from typing import Counter
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.sql.expression import false, null, true
from sqlalchemy.sql.functions import user
from sqlalchemy.sql.schema import Table
from database import Base
from sqlalchemy.orm import relation, relationship
import datetime
import uuid

#多對多設置虛擬關聯表
# pa_on = Table(
#     'pa_on',
#     Column('user_id_rt',String(50),ForeignKey('People_account.userId'))
# )


#模型(model)定義
class People_account(Base):
    __tablename__='people_account'
    user_Id = Column(String(50),nullable=False,primary_key=True)
    account = Column(String(50),nullable=False)
    password = Column(String(50),nullable=False)
    rloe_id = Column(String(50),ForeignKey("role.role_Id"),nullable=False)
    create_time = Column(DateTime,nullable=False)
    #於'主要modle'設置一對多的一 relationship("要建關聯的表",backref="自己")
    db_pa_pi = relationship("people_information",backref="people_account")
    db_pa_on = relationship("order_nomal",backref="people_account")
    db_pa_oc = relationship("order_customer",backref="people_account")

    def __init__(self,user_Id,account,password,role_id):
        self.user_Id=user_Id
        self.account=account
        self.password=password
        self.rloe_id=role_id
    
    def __repr__(self):
        return self.user_Id + '/' + self.account + '/' + self.password + '/' + self.rloe_id

class People_information(Base):
    __tablename__='people_information'
    information_Id = Column(String(50),nullable=False,primary_key=True)
    #於'次要model'設置一對多的'一' ForeignKey('建關聯的表.關聯欄位')
    user_Id = Column(String(50),ForeignKey('people_account.user_Id'),nullable=False)
    name = Column(String(50),nullable=False)
    phone = Column(String(50),nullable=False)


    def __init__(self,information_Id,user_Id,name,phone):
        self.information_Id=information_Id
        self.user_Id=user_Id
        self.name=name
        self.phone=phone
    
    def __repr__(self):
        return self.information_Id + '/' + self.userId + '/' + self.name + '/' + self.phone     


class Role(Base):
    __tablename__='role'
    role_Id = Column(Integer,nullable=False,primary_key=True)
    name = Column(String(50),nullable=False)

    db_role_pa = relationship("people_account",backref="role")

    def __init__(self,role_Id,name):
        self.role_Id = role_Id
        self.name = name

    def __repr__(self):
        return self.role_Id + '/' + self.name


class Product_nomal(Base):
    __tablename__='product_nomal'
    pn_Id = Column(String(50),nullable=False,primary_key=True)
    price = Column(String(50),nullable=False)
    name = Column(String(50),nullable=False)
    unit = Column(String(50),nullable=False)
    description = Column(String(50),nullable=False)
    img = Column(String(50),nullable=False)
    #一對多的一 relationship("要建關聯的表",backref="自己")
    db_pn_on= relationship("order_nomal",backref="product_nomal")

    def __init__(self,price,name,unit,description,img):
        self.pn_Id=str(uuid.uuid4)
        self.price=price
        self.name=name
        self.unit=unit
        self.description=description
        self.img=img

    def __repr__(self):
        return self.price + '/' + self.name + '/' + self.unit + '/' + self.description + '/' + self.img   

class Order_nomal(Base):
    __tablename__='order_nomal'
    on_Id = Column(String(50),nullable=False,primary_key=True)
    user_Id = Column(String(50),ForeignKey("people_account.user_Id"),nullable=False)
    #一對多的多 ForeignKey('建關聯的表.關聯欄位')
    pn_Id = Column(String(50),ForeignKey('product_nomal.pn_Id'),nullable=False)
    quantity = Column(String(50),nullable=False)
    total = Column(String(50),nullable=False)
    create_time = Column(DateTime,nullable=False)

    def __init__(self,on_Id,user_Id,pn_Id,quantity,total,create_time):
        self.on_Id=on_Id
        self.user_Id=user_Id
        self.pn_Id=pn_Id
        self.quantity=quantity
        self.total=total
        self.create_time=create_time

    def __repr__(self):
        return self.on_Id + '/' + self.user_Id + '/' + self.pn_Id + '/' + self.quantity + '/' + self.total + '/' + self.create_time


class Order_customer(Base):
    __tablename__ = 'order_customer'
    oc_Id = Column(Integer,nullable=False,primary_key=True)
    user_Id = Column(String(50),ForeignKey("people_account.user_Id"),nullable=False)
    ct_Id = Column(Integer,ForeignKey("cake_taste.ct_Id"),nullable=False)
    ci_Id = Column(Integer,ForeignKey("cake_inner.ci_Id"),nullable=False)
    ci_Id2 = Column(Integer,ForeignKey("cake_inner.ci_Id"),nullable=False)
    total = Column(Integer,nullable=False)
    create_time = Column(DateTime,nullable=False)

    def __init__(self,oc_Id,user_Id,ct_Id,ci_Id,ci_Id2,total,create_time):
        self.oc_Id = oc_Id
        self.user_Id = user_Id
        self.ct_Id = ct_Id
        self.ci_Id = ci_Id
        self.ci_Id2 = ci_Id2
        self.total = total
        self.create_time = create_time

    def __repr__(self):
        return self.oc_Id + '/' + self.user_Id + '/' + self.ct_Id + '/' + self.ci_Id + '/' + self.ci_Id2 + '/' + self.total + '/' + self.create_time


class Cake_inner(Base):
    __tablename__ = 'cake_inner'
    ci_Id = Column(Integer,nullable=False,primary_key=True)
    name = Column(String(50),nullable=False)
    price = Column(Integer,nullable=False)

    db_ci_oc = relationship("order_customer",backref="cake_inner")

    def __init__(self,ci_Id,name,price):
        self.ci_Id = ci_Id
        self.name = name
        self.price = price
    
    def __repr__(self):
        return self.ci_Id + '/' + self.name + '/' + self.price


class Cake_taste(Base):
    __tablename__ = 'cake_taste'
    ct_Id = Column(Integer,nullable=False,primary_key=True)
    name = Column(String(50),nullable=False)
    price = Column(Integer,nullable=False)

    db_ct_oc = relationship("order_customer",backref="cake_taste")

    def __init__(self,ct_Id,name,price):
        self.ct_Id = ct_Id
        self.name = name
        self.price = price
    
    def __repr__(self):
        return self.ct_Id + '/' + self.name + '/' + self.price