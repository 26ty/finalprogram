$(document).ready(function(){
    $("#output-btn").click(function(e){
        //判斷是否有空值
        if($("#name").val() == "" || $("#price").val() == "" || $("#description").val() =="" || $("#unit").val() =="" || $("#img").val()==""){
            //alert("")
            $.confirm({
                title: '注意！',
                animation: 'zoom',
                closeAnimation: 'scale',
                content: '未填寫商品資訊',
                buttons: {
                    確認: {
                        btnClass: 'btn-success',
                        action: function() {
                        }
                    }
                }
            })
        }else{
            $.ajax({
                type:"POST",
                url:"/namal-product-add?name=" + $("#name").val() + "&price=" + $("#price").val() + "&description=" + $("#description").val() + "&unit=" + $("#unit").val(),
                success: function(response){
                    $("#name").val(""),
                    $("#price").val(""),
                    $("#description").val(""),
                    $("#unit").val("")
                    $.confirm({
                        title: '成功！',
                        animation: 'zoom',
                        closeAnimation: 'scale',
                        content: '建立成功，首頁預覽吧！',
                        buttons: {
                            確認: {
                                btnClass: 'btn-success',
                                action: function() {
                                    location.href = "/"
                                }
                            }
                        }
                    })
                }
            });
        }
    })
});